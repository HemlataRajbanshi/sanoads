import { NavLink as Link } from 'react-router-dom';
import styled from 'styled-components';
import { FaSearch } from 'react-icons/fa';

export const Nav = styled.nav`
background: var(--primary-color, #000);
height: 85px;
display: flex;
z-index: 12;
`;

export const NavLink = styled(Link)`
color: var(--white-text-color, #F5F5F7);
font-size: 24px;
justify-content: flex-start; 
display: flex;
align-items: center;
text-decoration: none;
padding: 0 1rem;
height: 100%;
cursor: pointer;
&.active {
	color: #DFCBCB;
}
`;

export const NavBtn = styled.nav`
display: flex;
align-items: center;
margin-right: 24px;
justify-content: flex-end;
width: 100vw; */
@media screen and (max-width: 768px) {
	display: none;
}
`;

export const NavBtnLink = styled(Link)`
border-radius: 4vh;
background: #808080;
padding: 10px 22px;
color: #000000;
outline: none;
border: none;
cursor: pointer;
transition: all 0.2s ease-in-out;
text-decoration: none;
margin-left: 24px;
&:hover {
	transition: all 0.2s ease-in-out;
	background: #fff;
	color: #808080;
}
`;


export const SearchInput = styled.input`
padding: 8px;
border: 1px solid #ccc;
font-size: 16px;
width:100vh;
height:30px:
color: #D3A9A9;
font-size:16px;
`;

export const SearchIcon = styled(FaSearch)`
color: var(--search-icon-color, #CFB8B8);
font-size: var(--search-icon-size, 20vh);
`;
