import { FaSearch } from 'react-icons/fa';
import styled from 'styled-components';


const SearchBarContainer = styled.div`
display: flex;
align-items: center;
background-color: #FFFFFF;
border-radius: 4px;
padding: 0.5rem;
`;

const SearchInput = styled.input`
border: none;
outline: none;
background: transparent;
margin-left: 0.5rem;
font-size: 16px;
color: #D3A9A9;
`;

const SearchIcon = styled(FaSearch)`
color: var(--search-icon-color, #CFB8B8);
font-size: var(--search-icon-size, 20px);
`;

const SearchBar = () => {
	return (
    <SearchBarContainer>
		<SearchIcon />
        <SearchInput type="text" placeholder="Search..." />
    </SearchBarContainer>
    );
};

export default SearchBar;