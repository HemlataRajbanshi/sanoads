import React, { useState } from "react";
import './CreateAd.scss';
import axios from "axios";
import { Link } from "react-router-dom";
import { RxCross1} from "react-icons/rx";

const CreateAd =() =>{

    const [ad_Title, setTitle] = useState("");
    const [amt, setPrice] = useState("");
    const [contact, setContact] = useState("");
    const [loc, setLocation] = useState("");
    const [adType, setAdType] = useState("");
    const [desc, setDesc] = useState("");
    const [img1, setImg1] = useState("");
    const [img2, setImg2] = useState("");
    const [img3, setImg3] = useState("");

    const[AdStatus, setAdStatus ] = useState("");

    const createAd = (e) => {
        e.preventDefault();

        axios.post("http://localhost:4000/createAd",{
            ad_Title: ad_Title,
            price: amt,
            contact: contact,
            location: loc,
            adType: adType,
            description: desc,
            img1: img1,
            img2: img2,
            img3: img3,
        }).then((response) => {
            setAdStatus(response.data);
            console.log(response);
        })

    }

    return(
        <h className="Create_container">
            <div className="CreateForm">
                <div className="head">
                <div className="cross"> <Link to="/Home"> <RxCross1/></Link></div>
                <h3>Create an Ad</h3>
                </div>

                <div className="details">

                <div>
                    <label htmlFor="">Title of Ad</label>
                    <input type="text" placeholder="Title of Ad" name="ad_Title" onChange={(e) => {setTitle(e.target.value);}} required/>
                </div>

                <div>
                    <label htmlFor="">Price</label>
                    <input type="number" placeholder="Amount" name="amt" onChange={(e) => {setPrice(e.target.value);}} required />
                </div>

                <div>
                    <label htmlFor="">Contact</label>
                    <input type="number" placeholder="Contact" name="contact" onChange={(e) => {setContact(e.target.value);}} required />
                </div>

                <div>
                    <label htmlFor="">Location</label>
                    <input type="text" placeholder="Location" name="loc" onChange={(e) => {setLocation(e.target.value);}} required />
                </div>

                <div>
                    <label htmlFor="">Type of Advertisement</label>
                    <input type="select" placeholder="Select" name="adType"  onChange={(e) => {setAdType(e.target.value);}} required />
                </div>

                <div>
                    <label htmlFor="">Description</label>
                    <input type="text" placeholder="Description" name="desc" onChange={(e) => {setDesc(e.target.value);}} required />
                </div>
                
                <br/>
                <button className="pic" type="file" >Upload Picture</button>

                <br />

                <div className="pictures">
                    <input type="file" accept="image/*" name="img1" onChange={(e) => {setImg1(e.target.value);}} required />
                    <input type="file" accept="image/*" name="img2" onChange={(e) => {setImg2(e.target.value);}} required />
                    <input type="file" accept="image/*" name="img3" onChange={(e) => {setImg3(e.target.value);}} required />
                    
                </div>

                <div className="finalbtn">
                    <label type="button">Cancel</label>
                    <input type="submit" value="Submit"  onClick={createAd}/>
                </div>
                {AdStatus}
                </div>
            </div>
        </h>
    )
}

export default CreateAd;