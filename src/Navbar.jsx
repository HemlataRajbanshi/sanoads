 import React from "react";
import {Nav,NavLink,NavBtn,NavBtnLink,SearchInput,SearchIcon} from './NavbarElements';

const Navbar = () => {
return (
	<>
	<Nav>

		<NavLink to="/Home">
			LOGO
		</NavLink>

		<SearchInput type="search" placeholder="Search..." SearchIcon={SearchIcon} />

		<NavBtn>
			<NavBtnLink to="/CreateAd" >Create an ad</NavBtnLink>
		</NavBtn>

		<NavBtn>
			<NavBtnLink to="/Login" >Login In</NavBtnLink>
		</NavBtn>

		<NavBtn>
			<NavBtnLink to="/Register">Register</NavBtnLink>
		</NavBtn>

	</Nav>
	</>
);
};

export default Navbar;
