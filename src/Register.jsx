import React, {useState} from "react";
import { Link, useNavigate} from 'react-router-dom';
import './Register.scss';
import axios from 'axios';


const Register = () => {   
    
    const changeScreen = useNavigate();
    const [fname, setFname] = useState("");
    const [lname, setLname] = useState("");
    const [email, setEmail] = useState("");
    const [pno, setPno] = useState("");
    const [add, setAdd] = useState("");
    const [pw, setPw] = useState("");
    const [cpw, setCPw] = useState("");


    console.log(email);

    const [RegStatus, setRegStatus] = useState("");

    const register = (e) => {
        e.preventDefault();

        axios.post("http://localhost:4000/register", {
            fname: fname,
            lname: lname,
            email: email,
            phone_no: pno,
            address: add,
            password: pw,
            confirmPassword: cpw,
        }).then((response) => {
            setRegStatus(response.data);
            console.log(response);

            if(response.data === "User successfully registered"){
                changeScreen('/Login')
                alert(response.data);
            }
            else{
                alert(response.data);
            }

        })
    }

    return(
        <div className="register">

            <form action="" method="post">
            <h1>Register</h1>

                <div className="feilds">
                    <input type="text" placeholder="First Name" name="fname" onChange={(e)=> {setFname(e.target.value);}} required />
                    <input type="text" placeholder="Last Name" name="lname" onChange={(e)=> {setLname(e.target.value);}} required />
                    <input type="tel" placeholder="Phone Number" name="pno" onChange={(e)=> {setPno(e.target.value);}} required />
                    <input type="text" placeholder="Address" name="address" onChange={(e)=> {setAdd(e.target.value);}} required />
                    <input type="email" placeholder="Email" name="email" onChange={(e)=> {setEmail(e.target.value);}} required  />
                    <input type="password" placeholder="Password" name="pw" onChange={(e)=> {setPw(e.target.value);}} required />
                    <input type="password" placeholder="Confirm Password" name="cpw" onChange={(e)=> {setCPw(e.target.value);}} required />
                </div>

                <div className="regbtn">
                    <button onClick={register} type='submit'>
                        Register
                    </button> 
                </div>

                <div className="text">
                    <p>Forgot Your Password?</p>
                    <opt1>Already have an account? <Link to= "/Login">LogIn</Link></opt1>
                </div>
                {RegStatus}

            </form>

        </div>
    );
}

export default Register;