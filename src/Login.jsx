import React, { useState } from "react";
import axios from 'axios';
import { Link, useNavigate } from 'react-router-dom';
import './Login.scss';

const Login = () => {

    const changeScreen = useNavigate();
    const [email, setEmail] = useState("");
    const [pw, setPw] = useState("");

    console.log(email);

    const [loginStatus, setLoginStatus] = useState("");

    const login = (e) =>{
        e.preventDefault();

        axios.post("http://localhost:4000/login",{
            email: email,
            password: pw,
        }).then((response)=>{
            setLoginStatus(response.data);
            console.log(response)

            if(response.data === "Log in Successful"){
                changeScreen('/Home')
                alert(response.data);
            }
            else{
                alert(response.data);
            }

        })
    }

    return(
        <div className="login">
            <form action="" method="post">
                <h1>Login</h1>

                <div className="email">
                    <input type="email" placeholder="Username/ Email" name="email" value={email} id="email" onChange={(e) => {setEmail(e.target.value)}} required />
                </div>

                <div className="pw">
                    <input type="password" placeholder="Password" name="password" value={pw} id="pw" onChange={(e) => {setPw(e.target.value)}} required />
                </div>

                <div className="loginbtn">
                    <btn onClick={login} >
                        <button>
                            Login
                        </button> 
                    </btn>
                </div>
                <div className="text">
                    <p>Forgot Your Password?</p>
                    <opt>Don’t have an account? <Link to= "/Register">Register</Link></opt>
                </div>
                {loginStatus}

            </form>

        </div>
    );
}

export default Login;