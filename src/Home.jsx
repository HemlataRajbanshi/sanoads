import React, { useEffect , useState} from "react";
import Navbar from "./Navbar";
// import { Link, useNavigate } from 'react-router-dom';

function Home(){

    const [data, setData] = useState(null)

    useEffect(() => {
        fetch("http://localhost:8081/ads")
        .then((response) => {
            if(!response.ok){
                throw new Error("Failed to fetch data");
            }
            return response.json();
        })
        .then((data) => {
            setData(data);
        })
        .catch((error) =>{
            console.error("error fetching data:", error);
        });
    }, []);

    return(
        <div className="home">
            <Navbar/>

            <div className="adcollection">
                {data ? (
                    data.map((ads) => {
                        return(
                        <div className="ads" key={ads.Ad_Id}>
                            <h2>{ads.Ad_Title}</h2>
                            <h2>{ads.Price}</h2>
                            {/* <img src={ads.Image_1}alt="img" /> */}
                            <h2>{ads.Description}</h2>
                            <h2>{ads.Contact}</h2>
                            <h2>{ads.Location}</h2>
                        </div>
                        )
                    })
                ):(
                    <p>loding..... </p>
                )}
            </div>
        </div>
    );
}


export default Home;