import React from 'react';
import { BrowserRouter, Routes, Route} from 'react-router-dom';
import './App.css';
import Login from './Login';
import Register from './Register';
import Home from './Home';
import CreateAd from './CreateAd';

function App(){
  return(
    
  <BrowserRouter>
    <div>
      <Routes>
        <Route exact path="/" Component={Home} />
        <Route path="/Home" Component={Home} />
        <Route path="/Register" Component={Register} />
        <Route path="/Login" Component={Login} />
        <Route path="/CreateAd" Component={CreateAd} />
    </Routes>
    </div>

  </BrowserRouter> 

    
  );
}


export default App;
